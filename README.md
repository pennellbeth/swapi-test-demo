# Star Wars API Exploratory Testing Project

## Description

Using the Star Wars API ([https://swapi.dev/](https://swapi.dev/)), this repo
demonstrates different approaches to exploring a new API (particularly the
`/planets` endpoint in this case), using a mix of manual and automated
techniques.

It utilises the following:

- [Playwright Python](https://playwright.dev/python/docs/intro#installation)
- [pytest-playwright Plugin](https://playwright.dev/python/docs/test-runners)
- [Postman](https://www.postman.com/downloads/)
- [Newman](https://www.npmjs.com/package/newman)
- [GitLab CI](https://docs.gitlab.com/ee/ci/)

It is supported by documentation files outlining and breaking down the test
approach used, risks identified, coverage and exploration.

This is not intended to be an exhaustive list of all tests for this endpoint,
merely a demonstration of approach to exploring and learning, identification of
functionality requiring test, and how to integrate these tests into a CI/CD
pipeline.

## Getting Started

If you want to follow along with my approach to exploring the Star Wars API,
head to the `/docs` folder which has the following files to get started:

- [1. Getting Started](/docs/1_getting_started.md)
- [2. Refining Scope](docs/2_refining_scope.md)

If you'd like to run the test locally yourself:

### Playwright Tests

The `pytest.ini` file contains the default settings for running the Playwright
tests. Further options such as device emulation and slow-mo mode are in the
pytest help menu and can be passed in as command line arguments during local
test running.

So you can start running the tests in the repo by simply running `pytest`.

### Postman Tests

Further functional tests on the API have been conducted through Postman.

- Manual Testing - Import the collection file found in `test/postman`
  into your own Postman client or browser.
- Newman - The collection tests can either be run against Newman on your local
  machine with `newman run test/postman/swapi.postman_collection.json` or
  can be seen running in the Gitlab project CI.

## CI/CD

The above tests run in a GitLab CI/CD pipeline, the details for the stages of
which can be found in the `.gitlab-ci.yml` file.

Linting and pre-commit hooks have also been added to ensure consistency in the
code.

To set up the pre-commit hook, run `setup.sh`.
