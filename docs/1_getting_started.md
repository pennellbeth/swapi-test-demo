# Getting Started

This document provides context for approaching a previously unseen application -
in this case, the Star Wars API. For the purposes of this demo repo, the scope
has been limited to just the `/planets` endpoint, as the focus is primarily on
documenting the journey of exploration.

## What Are We Working With?

When approaching a new project, I begin with a broad landscape assessment. This
could include:

- Getting to grips with the application under test
- Application context
- Tech stack
- Context about the project / task / ticket being worked on
- Existing tooling / testing / experience within the stack / team

The context I'm operating in is key, as it greatly affects the test planning and
strategy that would be most effective. Where there's never enough time to test
all of the things, understanding the key quality aspects for the context at hand
guides the risk areas, which guide the mitigation strategy.

## Set Up

I have a basic template for exploring web applications which guides my thought
process, and I document my findings using mind maps.

[<img src="images/1a_exploration_start.PNG" width="500" height="250" target="_blank" />](images/1a_exploration_start.PNG)

This can always be tweaked where appropriate, but offers a general guide.

A lot of my exploration is guided by heuristics, from either my own experience
or from resources online such as:

- [Test Heuristics Cheat Sheet - Hendrickson, Lyndsay, Emery](http://testobsessed.com/wp-content/uploads/2011/04/testheuristicscheatsheetv1.pdf)
- [Testing Mnemonics - Del Dewar](https://findingdeefex.files.wordpress.com/2015/05/testingmnemonics1.jpg)
- [Test Heuristics Cheat Sheet - Simon Tomes, Ministry of Testing](https://www.ministryoftesting.com/dojo/lessons/test-heuristics-cheat-sheet)

## The Application Under Test

So with this ready I can have a look at the application, hosted at
`https://swapi.dev/`. I limit the initial exploration of the UI to 15-20
minutes, and update the mind map as I go.

(Note: This exploration actually ended up being nearer to 45 mins, with half for
exploration and half for note-cleanup/UI annotation for the purposes of this repo)

And just from the UI, there's some really interesting data pulled out.

[<img src="images/1b_ui_annotation.PNG" width="500" height="250" target="_blank" />](images/1b_ui_annotation.PNG)

I'm able to glean:

- The context for the application
- Its history (including previously being known as swapi.co when it was
  maintained by its original creator, Paul Hallett)
- The beginnings of target user personas
- Any UI elements of note, interactive or descriptive
- Further pages to continue exploring on

And so my exploration continues onto the 'About' and 'Documentation' pages, and
results in the mind map looking something like this:

[<img src="images/1c_ui_exploration.PNG" width="500" height="250" target="_blank" />](images/1c_ui_exploration.PNG)

Notes of interest are highlighted in blue, questions in orange; both can
influence potential future exploration topics.

## Recount, Refocus and Restart

From here, I can now use the mind map to begin directing my focus to other areas
of learning. I'm still establishing oracles[^1] to use when comparing the results of
my exploratory sessions, and so far I have the following listed:

- "Documentation" page of the UI
- GitHub repository for the application
- Wookiepedia

As the JSON schema on the UI is fairly comprehensive, I decide to take a look at
the GitHub repo for the application next, targeting the exploration towards the
API. Given that the application is using Django REST Framework, and the primary
examples are in Python, I'm expecting the vast majority of the repo to be in
python too.

Whilst not in a strict charter format, my thought process is trending that way
for the next session: "Explore API documentation with the GitHub repo to
discover more about how it is constructed"
I make a session for 15-20 mins, and update the mind map as follows:

[<img src="images/1d_github_notes.PNG" width="500" height="250" target="_blank" />](images/1d_github_notes.PNG)

I got some good notes from this, including more on the DB, how the data is
stored, imported, notes on the main API code files and how Wookiee translation
works. I note a small discrepancy in the Planets model which seems to be missing
the "residents" field present in the schema.

## Initial Review

At this point, within the first hour using the application, I've got a good
grasp on:

- The context of the application
- Initial user personas
- Structure of the codebase, tech stack and tooling
- Oracles for how the API is intended to function
- Potential avenues to explore next

---

#### Footnotes

[^1]: An oracle is a mechanism for determining whether what we observe during
      testing is "correct" or not. If you think of characters in films or TV
      series who time travel into a different decade than they intended -
      How do they determine that this is happened, or what year it is?
      They have a hunch something isn't right with where they've woken up, and
      this is then confirmed against a date on the top of the daily newspaper,
      or the registration plate on passing cars (e.g. XX01 XXX would be 2001),
      or just asking a passerby "WHAT YEAR IS THIS?!"
      All of these are oracles in determining whether they've accidentally
      woken up in 1952 or the year 3000.
