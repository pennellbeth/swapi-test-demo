# Refining Scope

From my initial exploration, I can use my mind map to form a number of
exploratory testing charters. These help not only to focus the areas of further
discovery, but also to prioritise what exploration will yield information we
care about.

Most of my charters are formed from questions I have about the service:

```html
|             Question              |                 Charter                  |
|-----------------------------------|------------------------------------------|
| How would this handle other REST  | Explore the planets endpoint with varied |
| methods/inputs?                   | methods and paths to discover how the API|
|                                   | handles inputs outside of the design spec|
|-----------------------------------|------------------------------------------|
| Can searching work on other schema| Explore the API search functionality with|
| fields and matching types?        | different fields and matching types to   |
|                                   | discover limits in matching capability   |
|-----------------------------------|------------------------------------------|
| Is the data being returned by the | Explore the planets endpoint with the    |
| API actually correct?             | underlying data files to discover any    |
|                                   | discrepancies in the data integrity      |
|-----------------------------------|------------------------------------------|
| What if this public API was       | Explore the rate limiting protection on  |
| targeted by abuse?                | the planets endpoint with high volumes of|
|                                   | messages to discover any vulnerabilities |
|                                   | to abuse the service                     |
|-----------------------------------|------------------------------------------|
| Does this application meet the    | Explore the planets endpoint with user   |
| needs of its intended user base?  | personas to assess usability and UX      |
|                                   | features of the design                   |
|-----------------------------------|------------------------------------------|
| How does the wookiee format       | Explore the wookiee format with a mix of |
| feature handle data with non-alpha| alpha/non-alpha characters to discover   |
| characters? (e.g. R2-D2)          | potential issues in translation logic    |
|-----------------------------------|------------------------------------------|
| How does the name search feature  | Explore the wookiee format with the      |
| work with the wookiee format?     | search functionality to discover any     |
|                                   | integration issues in the design         |
|-----------------------------------|------------------------------------------|
```

The above table was prioritised based on the most key functionality and common
use cases (utilising this service as a REST API, and using the search
functionality to access data), working down to the least critical functionality
(the wookiee format).

## Automated Exploratory Testing

As the first charter centres on input testing the planets endpoint, this
could be done using a number of methods; the query functionality in the UI,
using tools such as Postman to hit the API directly, or utilising the language
wrappers for the API.
For the purposes of this demo, I've opted for an automated exploratory testing
approach demonstrated by [Maaret Pyhäjärvi](https://www.youtube.com/watch?v=CQTuI5y2NFI).

Using pytest parameterisation and Playwright Python, a data-driven approach can
be taken to running a number of test cases, whilst having immediately codified
documentation for the exploration.

This approach utilises the UI, and refactors the test format as more tests are
conducted.
See `tests/playwright` for the versions of the test files.

### Approach

The main notes I'm keeping in mind from the mind map related to this charter are
as follows:

- Exploring the 3 endpoints listed in the documentation page, plus API root:
  - `/`
  - `planets/`
  - `planets/:id`
  - `planets/schema`
- GET is the only valid method
- No authentication is required for the API

`test_swapi_e1_v1.py` is the basic test case, utilising the UI selectors and
creating a framework for our future scenarios and assertions.

`test_swapi_e1_v1.py` then refactors this to use variables and parameterisation
to quickly iterate on scenarios, and adding ids to list scenario topics.

`test_swapi_e1_methods.py` looks at the combinations of endpoints and valid API
methods in order to create assertions that could not be handled within the UI
(which defaults to GET).

### Learnings and Outcomes

- Things working as expected:
  - Base `planets/` endpoint
  - Specific planet where integer id is within dataset
  - Error handling for alpha id values
  - Error handling for id values outside of dataset
- Things requiring clarification:
  - Should clicking in the text box wipe the placeholder text for access to the
    api root?
  - GET, HEAD and OPTIONS methods permitted (industry standard but worth asking
    given docs were explicit)
- Bugs found:
  - Inconsistent handling of whitespace in API path (permitted to access root
    but not endpoints)
  - `planets/schema` endpoint listed in documentation produces error
- Further notes:
  - `planets/` endpoint returns a paginated response of the first 10 planets
    returned in the list
  - There are query parameters other than search which are permissible - such as
    page number on the `planets/` endpoint (`?page=2`)

Bugs and discrepancies are noted as comments in the tests and marked with an
expected fail until fixed, or confirmed not to be an issue so the tests can be
updated.

### Next Steps

The playwright exploration primarily focused on accessing the API via the UI,
which inherently has the potential to introduce variation in results. For
example, whitespace being permitted in the UI to access the root API returns an
error when hitting the API directly (verified during exploration using Postman).

This can be explored further with a small Postman collection, and this can allow
for more targeted assertions for the responses of each endpoint than were
required for the generic approach taken during parameterising the scenarios.

See `tests/postman` for the collection itself.

Then, getting these tests running in the CI/CD pipeline for fast feedback during
continued development.
