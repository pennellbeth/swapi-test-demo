from typing import Generator

import pytest
from playwright.sync_api import (
    APIRequestContext,
    Playwright,
)


@pytest.fixture(scope="session")
def api_request_context(
    playwright: Playwright,
) -> Generator[APIRequestContext, None, None]:
    headers = {}
    request_context = playwright.request.new_context(
        base_url="https://swapi.dev/api/", extra_http_headers=headers
    )
    yield request_context
    request_context.dispose()


@pytest.mark.parametrize(
    "endpoint",
    [
        "./",
        "./planets/",
        "./planets/1/",
    ],
)
@pytest.mark.parametrize(
    "method",
    [
        "get",
        "head",
        "options",
    ],
)
def test_can_handle_get(
    api_request_context: APIRequestContext, method, endpoint
) -> None:

    request = api_request_context.fetch(endpoint, method=method)
    assert request.status == 200


@pytest.mark.parametrize(
    "endpoint",
    [
        "./",
        "./planets/",
        "./planets/1/",
    ],
)
@pytest.mark.parametrize(
    "method",
    [
        "post",
        "put",
        "patch",
        "delete",
        "copy",
        "link",
        "unlink",
        "purge",
        "lock",
        "unlock",
        "propfind",
        "view",
    ],
)
def test_cannot_handle_other_methods(
    api_request_context: APIRequestContext, method, endpoint
) -> None:

    request = api_request_context.fetch(endpoint, method=method)
    assert request.status == 405
