from playwright.sync_api import (
    Page,
    expect,
)

"""First test iteration, accessing the element selectors and making a basic
assertion to ensure we've got the right record back from the API (and that
the page has changed from the default response)"""


def test_expected_ui_response(page: Page):
    page.goto("/")
    page.fill("#interactive", "planets/1/")
    page.click("text=request")
    expect(page.locator("#interactive_output")).to_contain_text("Tatooine")
