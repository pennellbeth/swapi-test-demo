import pytest
from playwright.sync_api import (
    Page,
    expect,
)

"""Second test iteration, utilising variables and pytest parameterisation in order
 to run different data sets (scenarios) through the same test structure"""


@pytest.mark.parametrize(
    "input_text, exp_response",
    [
        # RAISE: Text not cleared on input>click, returns placeholder people/1/
        pytest.param(
            "",
            '"planets": "https://swapi.dev/api/planets/"',
            marks=pytest.mark.xfail,
        ),
        # BUG: Should not allow whitespace in API path, returns root
        pytest.param(" ", "404 error", marks=pytest.mark.xfail),
        (" planets/1/", "404 error"),
        # NOTE: Returns paginated response with first 10 planets
        ("planets/", '"count": 60'),
        ("planets/1/", '"name": "Tatooine"'),
        ("planets/60/", '"name": "Umbara"'),
        ("planets/100/", "404 error"),
        ("planets/foo/", "404 error"),
        # BUG: planets/schema/ valid endpoint in docs page, returns 404 error
        pytest.param(
            "planets/schema/",
            '"$schema": "http://json-schema.org/draft-04/schema"',
            marks=pytest.mark.xfail,
        ),
    ],
    ids=[
        "base root",
        "whitespace",
        "whitespace id",
        "base planets",
        "valid planet id",
        "last valid planet id - different page",
        "id not in dataset",
        "alpha planets id",
        "schema",
    ],
)
def test_expected_ui_response(page: Page, input_text, exp_response):
    page.goto("/")
    page.fill("#interactive", input_text)
    page.click("text=request")
    page.on("response", lambda response: print(response))
    expect(page.locator("#interactive_output")).to_contain_text(exp_response)
